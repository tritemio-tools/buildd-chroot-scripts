#!/bin/sh

CHROOT_DIR=tritemio-ubuntu-exp-bp
UBUNTU_SUITE=xenial

#set chroot
debootstrap --variant=buildd --include=eatmydata,ccache $UBUNTU_SUITE $CHROOT_DIR http://gb.archive.ubuntu.com/ubuntu

#set sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_SUITE main restricted universe multiverse" \
	> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_SUITE-security main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_SUITE-updates main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_SUITE-backports main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list

#echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_SUITE-proposed main restricted universe multiverse" \
#	>> $CHROOT_DIR/etc/apt/sources.list

cp /etc/hosts $CHROOT_DIR/etc/
echo "deb http://tritemio/tritemio ubuntu-exp-bp main" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb-src http://tritemio/tritemio ubuntu-exp-bp main" \
	>> $CHROOT_DIR/etc/apt/sources.list

#set tritemio keyring
chroot $CHROOT_DIR apt-get update
chroot $CHROOT_DIR apt-get -y --force-yes install tritemio-archive-keyring
chroot $CHROOT_DIR apt-get update

#set ccache
chroot $CHROOT_DIR dpkg-reconfigure ccache

#set apt preferences
cp preferences $CHROOT_DIR/etc/apt/preferences

#copy build-env.sh
cp ./build-env.sh $CHROOT_DIR
