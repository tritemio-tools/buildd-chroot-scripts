#!/bin/sh

CHROOT_DIR=tritemio-ubuntu-exp
UBUNTU_UNSTABLE_NAME=artful

#set chroot
debootstrap --variant=buildd --include=eatmydata,ccache $UBUNTU_UNSTABLE_NAME $CHROOT_DIR http://gb.archive.ubuntu.com/ubuntu

#set sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME main restricted universe multiverse" \
	> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-security main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-updates main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list

#echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-proposed main restricted universe multiverse" \
#	>> $CHROOT_DIR/etc/apt/sources.list

cp /etc/hosts $CHROOT_DIR/etc/
echo "deb http://tritemio/tritemio ubuntu-exp main" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb-src http://tritemio/tritemio ubuntu-exp main" \
	>> $CHROOT_DIR/etc/apt/sources.list

#set tritemio keyring
chroot $CHROOT_DIR apt-get --allow-insecure-repositories update
chroot $CHROOT_DIR apt-get -y --force-yes install tritemio-archive-keyring
chroot $CHROOT_DIR apt-get update

#set ccache
chroot $CHROOT_DIR dpkg-reconfigure ccache

#set apt preferences
cp preferences $CHROOT_DIR/etc/apt/preferences

#copy build-env.sh
cp ./build-env.sh $CHROOT_DIR

#Use apt-cacher-ng to get the packages.
echo "Acquire::http { Proxy \"http://127.0.0.1:3142\"; };" > $CHROOT_DIR/etc/apt/apt.conf.d/00-apt-proxy
