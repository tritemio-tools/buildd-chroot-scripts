#!/bin/sh

bn=`basename "$0"`
cwd=`pwd`

case $(dpkg-parsechangelog -S Source) in
"calligra")
	export DEB_BUILD_OPTIONS='parallel=1'
	;;
"plasma-desktop"|"rocs")
	export CCACHE_DISABLE='yes'
	export DEB_BUILD_OPTIONS='parallel=1'
	;;
"plasma-workspace")
	export DEB_BUILD_OPTIONS='parallel=2'
	;;
"ccache"|"karchive"|"kcodecs"|"kparts")
	export CCACHE_DISABLE='yes'
	export DEB_BUILD_OPTIONS='parallel=4'
	;;
*)
	export DEB_BUILD_OPTIONS='parallel=4'
	;;
esac

setup_ccache() {
        if [ -z "$CCACHE_DISABLE" ]; then
                export CCACHE_DIR=/var/cache/ccache-`dpkg-architecture -qDEB_HOST_ARCH`
                export CCACHE_UMASK=002
                export CCACHE_BASEDIR=`pwd`
                export CCACHE_COMPRESS=1
                export PATH="/usr/lib/ccache:$PATH"
	fi

}

echo_section() {
        local str count header
        str="$bn: $1"
        count=`echo -n "$str" | wc -c`
        header=""

        for i in `seq 1 $count`; do
                header="${header}-"
        done
        echo "$header" >&2
        echo "$str" >&2
        echo "$header" >&2
}

run_screen()
{
        if ! ( [ "$SBUILD_RUN_SCREEN" = "always" ] ||
               [ "$SBUILD_RUN_SCREEN" = "$1" ] ); then
                return 0
        fi

        if ! which screen > /dev/null; then
                echo "I: screen is not installed" >&2
                return 1
        fi

        local name="sbuild-`basename "$(pwd)"`-`date '+%Y%m%d-%H%M%S'`"
        local shell="`getent passwd "$USER" | cut -d: -f7`"
        if [ ! -e "$shell" ]; then
                shell="$SHELL"
                if [ ! -e "$shell" ]; then
                        shell="/bin/sh"
                fi
        fi

        echo_section "Starting screen session $name (shell: $shell)"
        screen -S "$name" -s "$shell" -D -m
        return 0
}

echo "I:" "$@" >&2

umask 002
setup_ccache

"$@"
bp_exitcode="$?"

#if [ -f debian/rules ]; then
#    echo_section "debian/rules list-missing output"
#    debian/rules list-missing
#    echo_section "debian/rules check-not-installed output"
#    debian/rules check-not-installed
#    echo_section "dh_install --list-missing output"
#    dh_install --no-act --list-missing
#else
#    echo_section "unable to find debian/rules"
#fi

if [ "$bp_exitcode" -eq 0 ]; then
        # Success
        run_screen "successful"
else
        # Failed. List debian/tmp
#        if [ -d debian/tmp ]; then
#                cd debian/tmp
#                echo_section "files in debian/tmp"
#                find . -type f
#                echo_section "symlinks in debian/tmp"
#                find . -type l -printf "%p -> %l\n"
#                cd "$cwd"
#        else
#            echo_section "debian/tmp does not exist"
#        fi
        run_screen "failed"
fi

exit $bp_exitcode
