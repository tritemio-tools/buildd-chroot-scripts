#!/bin/sh

ARCH=`dpkg-architecture -qDEB_HOST_ARCH`

#clear cache
CCACHE_DIR=cache/ccache-$ARCH/ ccache -C

#set size limit
CCACHE_DIR=cache/ccache-$ARCH/ ccache -M 10G

#reset stats
CCACHE_DIR=cache/ccache-$ARCH/ ccache -z
