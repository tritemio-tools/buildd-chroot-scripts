#!/bin/sh

CHROOT_DIR=ubuntu-unstable-autopkgtests
UBUNTU_UNSTABLE_NAME=artful

#PREINSTALLED=eatmydata,ccache,aptitude
PREINSTALLED=eatmydata,aptitude

BUILD_ENV_SCRIPT=build-env.sh_noccache
#BUILD_ENV_SCRIPT=build-env.sh


#set chroot
debootstrap --variant=buildd --include=$PREINSTALLED $UBUNTU_UNSTABLE_NAME $CHROOT_DIR http://gb.archive.ubuntu.com/ubuntu

#set sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME main restricted universe multiverse" \
	> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-security main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-updates main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list

#echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-proposed main restricted universe multiverse" \
#	>> $CHROOT_DIR/etc/apt/sources.list

cp /etc/hosts $CHROOT_DIR/etc/
echo "deb http://tritemio/tritemio ubuntu-exp main" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb-src http://tritemio/tritemio ubuntu-exp main" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://tritemio/tritemio ubuntu-exp2 main" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb-src http://tritemio/tritemio ubuntu-exp2 main" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://tritemio/tritemio ubuntu-exp3 main" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb-src http://tritemio/tritemio ubuntu-exp3 main" \
	>> $CHROOT_DIR/etc/apt/sources.list

#set tritemio keyring
chroot $CHROOT_DIR apt-get --allow-insecure-repositories update
chroot $CHROOT_DIR apt-get -y --force-yes install tritemio-archive-keyring
chroot $CHROOT_DIR apt-get update

#gcc/g++ 6
#chroot $CHROOT_DIR apt-get -y install software-properties-common
#chroot $CHROOT_DIR add-apt-repository -y ppa:ubuntu-toolchain-r/test
#chroot $CHROOT_DIR apt-get -y remove software-properties-common
#chroot $CHROOT_DIR apt-get -y autoremove --purge
#chroot $CHROOT_DIR apt-get update
#chroot $CHROOT_DIR apt-get -y dist-upgrade

#set ccache
#chroot $CHROOT_DIR dpkg-reconfigure ccache

#set apt preferences
#cp preferences $CHROOT_DIR/etc/apt/preferences

#copy build-env.sh
cp $BUILD_ENV_SCRIPT $CHROOT_DIR/build-env.sh

#delete the protocols file to avoid netbase failing to configure when
#preparing the autopkgtest testbed
chroot $CHROOT_DIR apt-get -y install netbase
rm -f $CHROOT_DIR/etc/protocols

#set permissions for shm
#this is needed by some autopkgtests using qtwebengine, otherwise they would fail like this
# Creating shared memory in /dev/shm/.org.chromium.Chromium.0uS7fM failed: Permission denied
# Unable to access(W_OK|X_OK) /dev/shm: Permission denied
# This is frequently caused by incorrect permissions on /dev/shm.  Try 'sudo chmod 1777 /dev/shm' to fix.
chroot $CHROOT_DIR chmod 1777 /dev/shm

#Use apt-cacher-ng to get the packages.
echo "Acquire::http { Proxy \"http://127.0.0.1:3142\"; };" > $CHROOT_DIR/etc/apt/apt.conf.d/00-apt-proxy
