#!/bin/sh

CHROOT_DIR=tritemio-ubuntu-exp3
UBUNTU_UNSTABLE_NAME=artful

PREINSTALLED=eatmydata,ccache,aptitude

BUILD_ENV_SCRIPT=build-env.sh_noccache
#BUILD_ENV_SCRIPT=build-env.sh


#set chroot
debootstrap --variant=buildd --include=$PREINSTALLED $UBUNTU_UNSTABLE_NAME $CHROOT_DIR http://gb.archive.ubuntu.com/ubuntu

#set sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME main restricted universe multiverse" \
	> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-security main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-updates main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list

#echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-proposed main restricted universe multiverse" \
#	>> $CHROOT_DIR/etc/apt/sources.list

cp /etc/hosts $CHROOT_DIR/etc/
echo "deb http://tritemio/tritemio ubuntu-exp3 main" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb-src http://tritemio/tritemio ubuntu-exp3 main" \
	>> $CHROOT_DIR/etc/apt/sources.list

#set tritemio keyring
chroot $CHROOT_DIR apt-get --allow-insecure-repositories update
chroot $CHROOT_DIR apt-get -y --force-yes install tritemio-archive-keyring
chroot $CHROOT_DIR apt-get update

#gcc/g++ 6
#chroot $CHROOT_DIR apt-get -y install software-properties-common
#chroot $CHROOT_DIR add-apt-repository -y ppa:ubuntu-toolchain-r/test
#chroot $CHROOT_DIR apt-get -y remove software-properties-common
#chroot $CHROOT_DIR apt-get -y autoremove --purge
#chroot $CHROOT_DIR apt-get update
#chroot $CHROOT_DIR apt-get -y dist-upgrade

#set ccache
chroot $CHROOT_DIR dpkg-reconfigure ccache

#set apt preferences
#cp preferences $CHROOT_DIR/etc/apt/preferences

#copy build-env.sh
cp $BUILD_ENV_SCRIPT $CHROOT_DIR/build-env.sh

#Use apt-cacher-ng to get the packages.
echo "Acquire::http { Proxy \"http://127.0.0.1:3142\"; };" > $CHROOT_DIR/etc/apt/apt.conf.d/00-apt-proxy
