#!/bin/sh

CHROOT_DIR=ubuntu-kci-unstable
UBUNTU_UNSTABLE_NAME=yakkety

#set chroot
eatmydata debootstrap --variant=buildd --include=eatmydata,ccache $UBUNTU_UNSTABLE_NAME $CHROOT_DIR http://gb.archive.ubuntu.com/ubuntu

#set sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME main restricted universe multiverse" \
	> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-security main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list
echo "deb http://gb.archive.ubuntu.com/ubuntu $UBUNTU_UNSTABLE_NAME-updates main restricted universe multiverse" \
	>> $CHROOT_DIR/etc/apt/sources.list

#set tritemio keyring
#chroot $CHROOT_DIR apt-get update
#chroot $CHROOT_DIR apt-get -y --force-yes install tritemio-archive-keyring
#chroot $CHROOT_DIR apt-get update

#Add kubuntu ci ppa
chroot $CHROOT_DIR apt-get -y install software-properties-common
chroot $CHROOT_DIR add-apt-repository -y ppa:kubuntu-ci/unstable
chroot $CHROOT_DIR apt-get -y remove software-properties-common
chroot $CHROOT_DIR apt-get -y autoremove --purge
chroot $CHROOT_DIR apt-get update
chroot $CHROOT_DIR apt-get -y dist-upgrade

#set ccache
chroot $CHROOT_DIR dpkg-reconfigure ccache

#set apt preferences
#cp preferences $CHROOT_DIR/etc/apt/preferences

#copy build-env.sh
cp ./build-env.sh $CHROOT_DIR
